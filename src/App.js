import {BrowserRouter,Switch,Route,Redirect} from 'react-router-dom'
import { Home } from './Pages/Home';
import { Card1 } from './Pages/Card1';
import { Card2 } from './Pages/Card2';
import { Card3 } from './Pages/Card3';
import { Card4 } from './Pages/Card4';
import { Card5 } from './Pages/Card5';
import { Navbar } from './Components/Navbar/navbar';


export const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/card1' component={Card1} />
      <Route exact path='/card2' component={Card2} />
      <Route exact path='/card3' component={Card3} />
      <Route exact path='/card4' component={Card4} />
      <Route exact path='/card5' component={Card5} />
      <Redirect to='/' />
    </Switch>
  </BrowserRouter>
  )
};
