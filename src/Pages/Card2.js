import { Profile } from "../Components/Card2/card";
import { Container } from "../Components/General/container";
import {data} from "../Components/General/data"

export const Card2 = () => {
    return (
        <Container>
            {data.map((persona, index) => {
                return(
                    <div key={index}>
                    <Profile
                    image={persona.Icon}
                    name={persona.name}
                    correo={persona.email}
                    cargo={persona.postion}
                    />
                    </div>
                )
            })
            }
        </Container>
    )
};

