import { Profile } from "../Components/Card1/card"
import { Container } from "../Components/General/container"
import {data} from "../Components/General/data"

export const Card1 = () => {
    return (
        <Container>
            {data.map((persona, index) => {
                return(
                    <div key={index}>
                    <Profile
                    image={persona.Icon}
                    position={persona.postion}
                    name={persona.name}
                    email={persona.email}
                    imagep={persona.Iconp}
                    />
                    </div>
                )
            })
            }
        </Container>
    )
};