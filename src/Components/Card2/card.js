import {CardComponent, ContenedorImg, FrontComponent, ImagenIcon, ImagePComponent, InfoFrontComponent, Legends, LegendsDiv, MenuComponent, Menucomponente, TextComponent, TextComponenteInfo, TextDivTest } from "./Compositioncard";
import { useState } from "react"
import {data} from "../General/data"
import IconInfo from "../../Assets/images/informacion.png"
import IconForma from "../../Assets/images/formacion.png"
import IconCv from "../../Assets/images/cvlac.png"
import IconRedes from "../../Assets/images/redes.png"


const datacolores = [
                      { id:0,
                        backcolor:"#CEE5D0",
                        Contenido:"Correo",
                        Contenido2:"Cargo"},
                      { id:1,
                        backcolor:"#F3F0D7",
                        Contenido:"CvLAC",
                        Contenido2:"CVLAC"},                      
                      { id:2,
                        backcolor:"#FED2AA",
                        Contenido:"Información",
                        Contenido2:"Información"},
                      { id:3,
                        backcolor:"#FFBF86",
                        Contenido:"Otros",
                        Contenido2:"Información"
                      }
        ];

export const Profile = (props) => {
    const [div, setdiv]=useState(0)  
    const resultado =datacolores.find( DivC => DivC.id === div );
    const resultado2 =data.find( Name => Name.name === props.name );

    switch(div){
      case 0:
        datacolores[div].Contenido=(resultado2.email);
        datacolores[div].Contenido2=(resultado2.postion);
        break;
      case 1:
        datacolores[div].Contenido=(resultado2.cvlac);
        datacolores[div].Contenido2=(resultado2.postion);
        break;
      case 2:
        datacolores[div].Contenido=(resultado2.info);
        datacolores[div].Contenido2=(resultado2.postion);
        break;
      case 3:
        datacolores[div].Contenido=(resultado2.otros);
        datacolores[div].Contenido2=(resultado2.postion);
        break;
      default:
        datacolores[0].Contenido=(resultado2.formacion);
        datacolores[div].Contenido2=(resultado2.postion);
        break;

    }
    /*
                          <TextComponent fontSize="15px">{props.correo}</TextComponent>
                      <TextComponent fontSize="15px">{props.cargo}</TextComponent>
    */ 
    return (
            <CardComponent>
              <FrontComponent>
                <InfoFrontComponent>
                  <ContenedorImg> <ImagePComponent  src={props.image}/> </ContenedorImg>
                    <TextComponenteInfo>
                      <TextComponent fontSize="18px" color="white">{props.name}</TextComponent>
                    </TextComponenteInfo>
                </InfoFrontComponent>
                  <Legends  backgroundColor={resultado.backcolor}>
                    <LegendsDiv> 
                    <TextDivTest>  <TextComponent fontSize="13.8px"> {resultado.Contenido} </TextComponent></TextDivTest>
                    <TextDivTest>{resultado.Contenido2}</TextDivTest>
                    </LegendsDiv>
                    </Legends>
              </FrontComponent>
              <MenuComponent>
                <Menucomponente onClick={()=> setdiv(0)}><ImagenIcon src={IconInfo} alt="Información"  /></Menucomponente>
                <Menucomponente onClick={()=> setdiv(1)}><ImagenIcon src={IconForma} alt="Información"  /></Menucomponente>
                <Menucomponente onClick={()=> setdiv(2)}><ImagenIcon src={IconCv} alt="Información"  /></Menucomponente>
                <Menucomponente onClick={()=> setdiv(3)}><ImagenIcon src={IconRedes} alt="Información"  /></Menucomponente>
              </MenuComponent>
            </CardComponent>
    )
};



