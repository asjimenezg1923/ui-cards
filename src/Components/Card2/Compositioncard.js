import styled from "styled-components";

//Contenedor general de la UI
export const CardComponent=styled.div`
    width: 18em; /* 400px */
	height: 24em; /* 600px */
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
    display: flex;
    flex-direction: column;
    position: relative;
    margin: 20px -10px;
    margin-bottom: 50px;

`

//Contenedor de los componentes de la UI
export const FrontComponent=styled.div`
    width: 100%;
	height: 100%;
	position: absolute;
	left: 0;
	top: 0;
	box-shadow: 2px 3px 5px black;
	overflow: hidden;
   // border-radius:24% 20% 0% 38% / 6% 23% 39% 20%   ;
   //border-radius: 50%;
   border-radius: 0% 20% 0% 38% / 0% 23% 39% 20% ;
   
`

//Información de la UI (Nombre, correo, cargo)
export const InfoFrontComponent=styled.div`
    width: 100%;
	height: 60%;
	text-align: center;
	background: #319ECC;
	position: relative;
	border-top-right-radius: 15px;
    &:after {
        content: "";
        width: 5px;
        height: 100%;
        position: absolute;
        top: 0;
        right: -50%;
        background: #F8E3B3 ;
        transition: right 1s;
        transform: skewX(-30deg);
    }
    ${FrontComponent}:hover &:after{
            right: 130%;
            transition-delay: 0.5s;
    }
`

//Imagen-Profile
export const ContenedorImg=styled.div`
    width: 150px;
    height: 150px;
    border-radius: 50%;
    margin-left: 25%;
	position: absolute;
    top:5px;
    background: white;
    box-shadow:0 8px 0 rgba(206, 201, 194), inset 0 -10px 0 rgba(70,255,255,0.25), 0 15px 0 rgba(0,0,0,0.50);
`
export const ImagePComponent=styled.img`
    width: 130px;
    position: relative;
    bottom: 0px;
    ${FrontComponent}:hover &{
        transform: scale(1.1);
        color: #2E5EAA;
    }

`

//Información de lo anterior mencionado
export const TextComponenteInfo=styled.div`
    width: 100%;
	height: auto;
	position: absolute;
	left: 0%;
	bottom: 15px;
    font-family: 'Montserrat', sans-serif;

`
//Textos
export const TextComponent=styled.p`
    margin: 0;
	padding: 0;
	margin-top: 3px;
    text-align: center;
	color: ${props => props.color};
    font-size: ${props => props.fontSize} ;

`

//Contenedores de la otra información (General)
export const Legends=styled.div`
    align-items: center;
    justify-content: center;
    display: flex;
    width: 100%;
	height: 40%;
	position: absolute;
    background-color: ${props => props.backgroundColor};
    

`

export const LegendsDiv=styled.div`
    width: 80%;
    height: auto;
    background-color:transparent;
    display:grid;
    align-items: center;
    justify-content: row;
    padding-left:10px;
    padding-bottom: 20px;


`
export const TextDivTest=styled.li`
    list-style: none;
    width: 220px;
    align-items: center;
    display: flex;
    justify-content: center;
    padding-top:15px ;
    text-align: center;
    color:#031240;
    font-weight: bold;
  
`
//Menu al lado
export const MenuComponent=styled.div`
 
    width: 80px;
    height: auto;
	top: 10px;
	right: -80px;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	perspective: 600px;
	transform-origin: left center;
	transform: rotateY(90deg);
	transition: all 1s;
	position: absolute;
    border-radius: 75% 25% 75% 25% / 32% 68% 32% 68%;
    ${CardComponent}:hover &{
        transform: rotateY(0deg);
    }
    
`

export const Menucomponente=styled.a`
    margin-bottom: 2px;
	width: 100%;
	height: 55px;
	line-height: 55px;
	color: #1C350B;
    background: rgb(127,241,246);
    background: linear-gradient(90deg, rgba(127,241,246,0.2102591036414566) 0%, rgba(242,122,41,0.1514355742296919) 100%);
	text-align: center;
	border-bottom-right-radius: 10px;
	transform: rotateY(90deg);
	transition: all 2.2s;
	transform-origin: left center;
    transition-property:border-radius;
    font-size: 15px;
    margin-left: 8px;
    ${CardComponent}:hover &{
        border-radius: 75% 25% 75% 25% / 32% 68% 32% 68%;
        transform: rotateY(0deg);
        
    }
    &:hover:nth-child(1){background: #F17F29;}
    &:hover:nth-child(2){background: #9C95DC;}
    &:hover:nth-child(3){background: #7DE2D1;}
    &:hover:nth-child(4){background: #EB5E55;}
`
export const ImagenIcon=styled.img`
    width: 40px;
    margin-top:8px;

`

