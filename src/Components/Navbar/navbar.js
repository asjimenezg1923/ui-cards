import styled from "styled-components";
import { NavLink as Link } from "react-router-dom";
import logo from "../../Assets/images/cinndet-16.png"

const NavbarComponentContainer=styled.nav`
    background: linear-gradient(90deg,#fff,#ece9e6);
    width: 1280px;
    display: grid;
    align-content: center;
    justify-content: space-between;
    grid-template-areas: "LogoUpn NavMenu";
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    margin: 20px auto 0;
`

const NavMenu=styled.div`
    grid-area: NavMenu;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr;
    grid-auto-flow: column;
    justify-self: center;
    align-items: center;
    grid-gap: 70px;
    margin: 20px; 
`
const LogoNavComponent = styled.div`
    grid-area: LogoUpn;
    width: 20px;
    margin-left: 30px;
    margin-top: 20px;
`
const Navitem=styled.li`
    font-size: 20px;
    font-weight: 550;
    color: #09456d;
    list-style-type: none;    
    text-align: center;
`
const NavLink=styled(Link)` text-decoration: none; `

export const Navbar = () => {
    return (
        <>
        <NavbarComponentContainer>
                <NavLink to='/'>
                    <LogoNavComponent><img className="Logo" src={logo} width="400px" alt="Logo" /></LogoNavComponent>
                </NavLink>
                <NavMenu>
                    <NavLink to='/card1'><Navitem>Card 1</Navitem></NavLink>
                    <NavLink to='/card2'><Navitem>Card 2</Navitem></NavLink>
                    <NavLink to='/card3'><Navitem>Card 3</Navitem></NavLink>
                    <NavLink to='/card4'><Navitem>Card 4</Navitem></NavLink>
                    <NavLink to='/card5'><Navitem>Card 5</Navitem></NavLink>
                </NavMenu>
        </NavbarComponentContainer>
        </>
    )
};
