import Male from "../../Assets/images/templatemale.png"
import Female from "../../Assets/images/templatefemale.png"
import Coordinador from "../../Assets/images/coordinate.png"
import CoordinadorTech from "../../Assets/images/coordination.png"
import WebDevelop from "../../Assets/images/seo.png"
import Webdesign from "../../Assets/images/web-design.png"
import Adplatform from "../../Assets/images/digital.png"
import Content from "../../Assets/images/content.png"
import Secretary from "../../Assets/images/receptionist.png"
import Assistant from "../../Assets/images/shop-assistant.png"

export const data = [
    {
        id:0,
        name:'Carlos Hernán López Ruiz',
        postion:' Coordinador General', 
        email:'clopez@pedagogica.edu.co',
        formacion:'F1',
        cvlac:'C1',
        info:'I1',
        otros:'O1',
        Iconp:Coordinador,
        Icon:Male
    },
    {
        id:1,
        name:'Ligia Lozano Cifuentes',
        postion:'Coordinadora Acividades Tecnológicas',
        email:'lclozano@pedagogica.edu.co',
        formacion:'F2',
        cvlac:'C2',
        info:'I2',
        otros:'O2',
        Iconp:CoordinadorTech,
        Icon:Female
    },
    {
        id:2,
        name:'Jorge Leonador Hernández Rozo',
        postion:'Desarrollador Web',
        email:'jlhernandezr@pedagogica.edu.co',
        formacion:'F3',
        cvlac:'C3',
        info:'I3',
        otros:'O3',
        Iconp:WebDevelop,
        Icon:Male
    },
    {
        id:3,
        name:'Johann Mateo Soler López',
        postion:'Diseñador E-learning',
        email:'jmsolerl@upn.edu.co',
        formacion:'F4',
        cvlac:'C4',
        info:'I4',
        otros:'O4',
        Iconp:Webdesign,
        Icon:Male
    },
    {
        id:4,
        name:'Diana Marcela Sánchez Yáñez',
        postion:'Administradora Plataforma LMS',
        email:'dmsanchezy@pedagogica.edu.co',
        formacion:'F5',
        cvlac:'C5',
        info:'I5',
        otros:'O5',
        Iconp:Adplatform,
        Icon:Female
    },
    {
        id:5,
        name:'Jhonny Alexander Ortegón Moreno',
        postion:'Gestor de Contenidos',
        email:'dqu_jaortegonm361@pedagogica.edu.co',
        formacion:'F6',
        cvlac:'C6',
        info:'I6',
        otros:'O6',
        Iconp:Content,
        Icon:Male
    },
    {
        id:6,
        name:'Sandra Castañeda Valdés ',
        postion:'Secretaria',
        email:'castaned@pedagogica.edu.co',
        formacion:'F7',
        cvlac:'C7',
        info:'I7',
        otros:'O7',
        Iconp:Secretary,
        Icon:Female
    },
    {
        id:7,
        name:'Camila Prontón Leguizamón ',
        postion:'Asisten Administrativa',
        email:'cpontonl@upn.edu.co',
        formacion:'F8',
        cvlac:'C8',
        info:'I8',
        otros:'O8',
        Iconp:Assistant,
        Icon:Female
    }

];
