import styled from 'styled-components'

const ContainerComponent=styled.div`	
    width: 1280px;
    min-height: 100vh;
    background: linear-gradient(90deg,#fff,#ece9e6);
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    margin: 0px auto;
    display: grid;
    justify-content: center;
    align-content: center;
    grid-template-columns: 400px 400px 400px;
    grid-template-rows: auto;
    grid-gap: 10px;
`


export const Container = (props) => {
    return(
        <ContainerComponent>
            {props.children}
        </ContainerComponent>
    )
}

