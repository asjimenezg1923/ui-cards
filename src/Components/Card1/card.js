import styled from "styled-components";
import { ComponentCardD, Email, Icon, IconP, Imagediv, ImageP, Name, Position } from "./Compositioncard";
import Iconmail from '../../Assets/images/mail.png'

const Cube=styled.div`
    position: relative;
    width: 350px;
    height: 350px;
    border-radius: 50% 50% 50% 50% / 30% 30% 70% 70%  ;
    transform-style: preserve-3d;
    align-items: center;
	text-align: center;
    transition: transform 1s; /* Animate the transform properties */
    margin: 0 auto;
    &:hover{ transform: rotateX(90deg); /* Text bleed at 90º */ 
    }
`


export const Profile = (props) => {
    return (
        <Cube>
            <ComponentCardD padding="20px 15px" transform="translateZ(50px)">
                <Imagediv> <Icon src={props.image}/> </Imagediv>
                <Name > {props.name} </Name>
                <Position>{props.position}</Position>
            </ComponentCardD>
            <ComponentCardD padding="50px 15px" transform="rotateX(-90deg) translateZ(0px);">
                <ImageP><IconP src={props.imagep} ></IconP></ImageP>   
                <Position>{props.position}</Position>
                <img src={Iconmail} alt="Mail" width="20px"></img>
                <Email>{props.email}</Email>
                </ComponentCardD>
        </Cube>
    )
};



