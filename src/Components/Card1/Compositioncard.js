import styled, {keyframes} from "styled-components";

//Animation Back
const BackAnimation=keyframes`
    0%{
        transform: rotate(0deg);
    }
    100%{
        transform: rotate(360deg);
    }
`
//Componente general del Front y Back
export const ComponentCardD=styled.div`
    position: relative;
    width: 350px;
    height: 350px;
    background: #fff;
    margin: 30px 10px;
    display: flex;
    flex-direction: column;
    background: #dcdde1;
    transition: 0.3s ease-in-out;
    align-items: center;
    border-radius: 50% 50% 50% 50% / 30% 30% 70% 70%  ;
    overflow: hidden;
    z-index: 1;
    padding: ${props => props.padding};
    transform: ${props => props.transform};
    /*word-break:break-all;*/ 
    &:before{
        content: '';
        position: absolute;
        width: 500px;
        height: 350px;
        clip-path: polygon(25% 0%, 100% 0%, 75% 100%, 0% 100%);
        background: linear-gradient(#00ccff,#d400d4);
        animation:${BackAnimation} 4s linear infinite;
        z-index: -1;
    }
    &:after{
        content: '';
        position: absolute;
        width: 350px;
        height: 350px;
        background: white;
        align-items: center;
        inset: 3px;
        border-radius: 50% 50% 50% 50% / 30% 30% 70% 70%  ;
        z-index:-1;
    }
`

export const Imagediv=styled.div`
    width: 200px;
    height: 200px;
    clip-path: polygon(20% 0%, 80% 0%, 100% 20%, 100% 80%, 80% 100%, 20% 100%, 0% 80%, 0% 20%);
`

//Name
export const Name=styled.h4`
    color: black;
    font-size: 15px;
    position: relative;
    text-align: center;
    transition: 0.3s ease-in-out;
    top:-20px;
`

//Position
export const Position=styled.h2`
    justify-self: center;
    color: #09456d;
    text-align: center;
    font-size: 15px;
    width: 200px;
    position: relative;
    top:-30px;
    text-transform: uppercase;
    font-size: 15px;
    text-shadow:    0px 0px 0 rgb(-136,17,119),
                    0px -1px  0 rgb(-272,-119,0),
                    0px -2px 1px rgba(9,69,109,0.28),
                    0px -2px 1px rgba(9,69,109,0.5),
                    0px 0px 1px rgba(9,69,109,.2); 
`
//Email
export const Email=styled.p`
    padding: 7px 0 0;
    color: #09456d;
    font-size: 15px;
    height: 70px;
`
//Div integrante
export const ImageP=styled.div`
    width: 80px;
    height: 80px;
    margin-bottom: 20px;
`
//Imagenes
export const IconP=styled.img`
    width: 100%;
    height: 100%;
`

export const Icon=styled.img`
    justify-self: center;
    height: 100%;
    width: 100%;
    z-index: 1;
    &:hover{
        transform: scale(1.2,1.2);
    }
`
